
#!/usr/bin/python
#Author: Raymond Mintz
#Email : raymondmintz@mf5tech.com
#TODO:
#	This program is not leaving camel case, its actually making EVERYTHING in 
#	string lowercase. Fix later.

#Change sub dirs also

import sys, os, re

currentDir = os.getcwd()

def doTheThing(theDir):
	changeCount=0
	pattern = re.compile('^[A-Z]')
	currentFile=""
	
	myFiles=os.listdir(theDir)
	
	for i in myFiles:
		
		currentFile = i
		
		if(pattern.match(currentFile)):
			print "Renaming {0} to {1}\n".format(currentFile, currentFile.lower())
			os.rename(currentFile, currentFile.lower())
			changeCount+=1
		
		print "Total changes %d" % changeCount
		
	print "END OF PROGRAM"

def main():
	option = raw_input("The current directory is: %s\n REALLY WANT TO CONTINUE? [y | n] " % currentDir)

	option = option.lower()

	if(option == "y"):
		doTheThing(currentDir)
	else:
		print 'Quitting'
		quit()

if __name__ == "__main__":
	main()
		

	

